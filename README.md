# Study of execution times on different platforms

This repository is used to map the amount of "Kilo-Whetstone" in microseconds on two different platforms:\

1. An Ada Ravenscar [runtime](https://gitlab.com/thesisBottaroMattia/ada-ravenscar-runtime-for-zynq7000-dual-core-supporting-mixed-criticality-systems) supporting semi-partitioned MCS dual-core model for zynq7000.
2. A set of RTEMS partitions running on [XtratuM](https://fentiss.com/products/hypervisor/). Each partitions runs a set of (ravenscar) tasks related to an Ada application.

These evaluations are functional for [my Master's Thesis work](https://gitlab.com/thesisBottaroMattia/mcs-vs-tsp-a-comparison).
